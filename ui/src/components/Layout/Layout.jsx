import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import NavigationRefresh from 'material-ui/svg-icons/navigation/refresh';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { blue300, blue400 } from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: blue300,
        primary2Color: blue400,
    },
});

function Layout({ children, socketStatus, onRefreshClick }) {
    return (
        <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <AppBar
                    title="dev-dashboard"
                    iconElementLeft={<span />}
                    iconElementRight={<FlatButton
                        onClick={onRefreshClick}
                        label={socketStatus}
                        labelPosition="before"
                        icon={<NavigationRefresh />}
                    />}
                />
                {children}
            </div>
        </MuiThemeProvider>
    );
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
    socketStatus: PropTypes.string.isRequired,
    onRefreshClick: PropTypes.func.isRequired,
};

export default Layout;
