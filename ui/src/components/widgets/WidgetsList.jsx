import React from 'react';
import PropTypes from 'prop-types';
import { GridList, GridTile } from 'material-ui/GridList';
import { blue200, white } from 'material-ui/styles/colors';
import NavigationRefresh from 'material-ui/svg-icons/navigation/refresh';
import { IconButton } from 'material-ui';
import Widget from './Widget';

const styles = {
    root: {
        marginTop: '20px',
    },
    gridTile: {
        paddingTop: '60px',
        paddingBottom: '20px',
        textAlign: 'center',
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    gridTileTitle: {
        fontSize: '24px',
    },
    gridTileTitleIcon: {
        color: white,
    },
};

function WidgetsList({ widgets, onRefreshClick }) {
    const grids = [1];

    widgets.forEach((widget) => {
        if (grids.indexOf(widget.layout.gridNumber) < 0) {
            grids.push(widget.layout.gridNumber);
        }
    });

    return (
        <div style={styles.root}>
            {grids.map(gridId => (
                <GridList
                    key={gridId}
                    style={{ width: `${95 / grids.length}%`, margin: `0 ${5 / (2 * grids.length)}%`, float: 'left' }}
                    cols={4}
                    padding={20}
                    cellHeight={180}
                >
                    {widgets.filter(widget => !widget.layout.gridNumber || widget.layout.gridNumber === gridId).map(widget => (
                        <GridTile
                            key={widget.id}
                            title={widget.layout.title}
                            actionIcon={<IconButton onClick={() => onRefreshClick(widget)} iconStyle={styles.gridTileTitleIcon}><NavigationRefresh /></IconButton>}
                            actionPosition="right"
                            style={styles.gridTile}
                            cols={widget.layout.cols || 1}
                            rows={widget.layout.rows || 1}
                            titlePosition="top"
                            titleBackground={blue200}
                            titleStyle={styles.gridTileTitle}
                        >
                            <Widget {...widget} />
                        </GridTile>
                    ))}
                </GridList>
            ))}
        </div>
    );
}

WidgetsList.defaultProps = {
    config: {},
};

WidgetsList.propTypes = {
    widgets: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        initialized: PropTypes.bool.isRequired,
        data: PropTypes.any.isRequired,
        layout: PropTypes.shape({
            title: PropTypes.string.isRequired,
            widget: PropTypes.string.isRequired,
            rows: PropTypes.number,
            cols: PropTypes.number,
            gridNumber: PropTypes.number,
            alertForMoreThan: PropTypes.number,
        }).isRequired,
        config: PropTypes.shape(),
    }).isRequired).isRequired,
    onRefreshClick: PropTypes.func.isRequired,
};

export default WidgetsList;
