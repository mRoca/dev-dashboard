import React from 'react';
import PropTypes from 'prop-types';

function WidgetImage({ id, data }) {
    return (
        <img title={id} src={data} alt={id} style={{ width: '100%', height: '100%' }} />
    );
}

WidgetImage.propTypes = {
    id: PropTypes.string.isRequired,
    data: PropTypes.string.isRequired,
};

export default WidgetImage;
