import React, { Component } from 'react';
import PropTypes from 'prop-types';
import WidgetLoading from './WidgetLoading';
import WidgetNumber from './WidgetNumber';
import WidgetListItem from './WidgetListItem';
import WidgetChartBarAndLine from './WidgetChartBarAndLine';
import WidgetIframe from './WidgetIframe';
import WidgetChartPieAndDoughnut from './WidgetChartPieAndDoughnut';
import WidgetImage from './WidgetImage';

class Widget extends Component {
    constructor(props) {
        super(props);

        this.availableWidgets = {
            number: WidgetNumber,
            listItem: WidgetListItem,
            chartBar: WidgetChartBarAndLine,
            chartLine: WidgetChartBarAndLine,
            chartPie: WidgetChartPieAndDoughnut,
            chartDoughnut: WidgetChartPieAndDoughnut,
            iframe: WidgetIframe,
            image: WidgetImage,
        };
    }

    render() {
        if (!this.props.initialized) {
            return <WidgetLoading id={this.props.id} data={this.props.data} />;
        }

        if (typeof this.availableWidgets[this.props.layout.widget] === 'undefined') {
            throw new Error(`Invalid ${this.props.layout.widget} widget type.`);
        }

        const UsedComponent = this.availableWidgets[this.props.layout.widget];

        return (
            <UsedComponent {...this.props} />
        );
    }
}

Widget.propTypes = {
    id: PropTypes.string.isRequired,
    initialized: PropTypes.bool.isRequired,
    layout: PropTypes.shape({
        title: PropTypes.string.isRequired,
        widget: PropTypes.string.isRequired,
        alertForMoreThan: PropTypes.number,
    }).isRequired,
    data: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.array,
        PropTypes.shape(),
    ]).isRequired,
};

export default Widget;
