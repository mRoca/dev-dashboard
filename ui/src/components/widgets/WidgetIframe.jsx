import React from 'react';
import PropTypes from 'prop-types';

function WidgetIframe({ id, data }) {
    return (
        <iframe title={id} src={data} style={{ width: '100%', height: '100%' }} frameBorder={0} />
    );
}

WidgetIframe.propTypes = {
    id: PropTypes.string.isRequired,
    data: PropTypes.string.isRequired,
};

export default WidgetIframe;
