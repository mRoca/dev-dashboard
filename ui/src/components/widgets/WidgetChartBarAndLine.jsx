import React from 'react';
import PropTypes from 'prop-types';
import { blue500, green300, red300 } from 'material-ui/styles/colors';
import { Bar, Line } from 'react-chartjs-2';

function WidgetChartBarAndLine({ data, layout }) {
    const chartData = {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: [],
            pointBackgroundColor: [],
            borderWidth: 0,
            pointRadius: 2,
        }],
    };

    const chartOptions = {
        animation: false,
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [{
                ticks: {},
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                },
            }],
        },
    };

    Object.keys(data).forEach((label) => {
        chartData.labels.push(label);
        chartData.datasets[0].data.push(data[label]);

        let color = blue500;

        if (typeof layout.alertForMoreThan !== 'undefined') {
            color = (data[label] > layout.alertForMoreThan) ? red300 : green300;
        }

        chartData.datasets[0].backgroundColor.push(color);
    });

    let UsedComponent = Bar;

    if (layout.widget === 'chartLine') {
        UsedComponent = Line;
        chartData.datasets[0].pointBackgroundColor = chartData.datasets[0].backgroundColor;
        delete chartData.datasets[0].backgroundColor;
    }

    return (
        <UsedComponent data={chartData} options={chartOptions} />
    );
}

WidgetChartBarAndLine.propTypes = {
    layout: PropTypes.shape({
        alertForMoreThan: PropTypes.number,
    }).isRequired,
    data: PropTypes.shape().isRequired,
};

export default WidgetChartBarAndLine;
