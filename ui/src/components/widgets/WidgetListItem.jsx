import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { grey400, grey900 } from 'material-ui/styles/colors';

class WidgetListItem extends Component {
    render() {
        const layout = this.props.layout;
        const data = this.props.data;

        const now = moment();
        const start = moment(layout.startDate);
        const duration = moment.duration(layout.duration);
        const itemsCount = data.length;

        if (!moment(layout.startDate).isValid()) {
            throw new Error('Invalid start date');
        }

        if (duration.toISOString() === 'P0D') {
            throw new Error('Invalid duration');
        }

        setTimeout(() => this.setState({}), duration.asMilliseconds() + 1000);

        if (!itemsCount) {
            return <div style={{ fontSize: 50, color: grey400 }}>No data</div>;
        }

        if (start > now) {
            return <div style={{ fontSize: 50, color: grey400 }}>Not yet</div>;
        }

        let iterCount = 0;
        const curIterDate = start.clone();
        while (now > curIterDate.add(duration) && iterCount < 9999999) {
            iterCount++;
        }

        return (
            <div>
                <div style={{ fontSize: 50, color: grey900 }}>{data[iterCount % itemsCount]}</div>
                {layout.displayNext &&
                <div style={{ fontSize: 40, color: grey400 }}>{data[(iterCount + 1) % itemsCount]}</div>
                }
            </div>
        );
    }
}

WidgetListItem.propTypes = {
    layout: PropTypes.shape({
        duration: PropTypes.string, // ISO 8601 duration
        startDate: PropTypes.string, // ISO 8601 date
    }).isRequired,
    data: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default WidgetListItem;
