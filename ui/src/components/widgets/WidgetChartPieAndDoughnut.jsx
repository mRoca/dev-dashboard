import React from 'react';
import PropTypes from 'prop-types';
import { Doughnut, Pie } from 'react-chartjs-2';
import * as availableColors from 'material-ui/styles/colors';

const colorVariance = '300';
const colors = ['red', 'green', 'pink', 'blue', 'yellow', 'brown', 'orange', 'purple', 'amber', 'teal', 'indigo', 'lightBlue', 'deepOrange', 'grey', 'lime', 'brown', 'lightGreen'];
const font = 'sans-serif';

function getColor(color) {
    return availableColors[color + colorVariance] || availableColors[color] || color;
}

function WidgetChartPieAndDoughnut({ data, layout }) {
    const chartData = {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: [],
        }],
    };

    const chartOptions = {
        animation: false,
        maintainAspectRatio: false,
        elements: {
            centerText: {
                text: Object.values(data).reduce((sum, v) => sum + parseFloat(v), 0),
                color: getColor('grey400'),
            },
        },
    };

    if (typeof layout.alertForMoreThan !== 'undefined') {
        chartOptions.elements.centerText.color = getColor(chartOptions.elements.centerText.text > layout.alertForMoreThan ? 'red' : 'green');
    }

    const labels = Object.keys(data);
    if (layout.orderedColors && layout.colors) {
        labels.sort((a, b) => (Object.keys(layout.colors).indexOf(a) < Object.keys(layout.colors).indexOf(b) ? -1 : 1));
    }

    labels.forEach((label) => {
        chartData.labels.push(label);
        chartData.datasets[0].data.push(data[label]);

        let color = getColor(colors[chartData.labels.length % colors.length]);

        if (typeof layout.colors !== 'undefined' && layout.colors[label]) {
            color = getColor(layout.colors[label]);
        }

        chartData.datasets[0].backgroundColor.push(color);
    });

    const ChartComponent = layout.widget === 'chartDoughnut' ? Doughnut : Pie;

    const plugins = [{
        beforeDraw(chart) {
            // Add a text label in the center of Doughnuts charts

            if (!chart.config.options.elements.centerText) {
                return;
            }

            const ctx = chart.chart.ctx;
            const config = Object.assign({}, { text: '', color: availableColors.black }, chart.config.options.elements.centerText);

            ctx.font = `30px ${font}`;

            const paddingRatio = 0.3;
            const widthRatio = ((2 - (2 * paddingRatio)) * chart.innerRadius) / ctx.measureText(config.text).width;
            const fontSizeToUse = Math.min(Math.floor(30 * widthRatio), 2 * chart.innerRadius);

            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.font = `${fontSizeToUse}px ${font}`;
            ctx.fillStyle = config.color;

            ctx.fillText(config.text, (chart.chartArea.left + chart.chartArea.right) / 2, (chart.chartArea.top + chart.chartArea.bottom) / 2);
        },
    }];

    return (
        <ChartComponent data={chartData} options={chartOptions} plugins={plugins} redraw />
    );
}

WidgetChartPieAndDoughnut.propTypes = {
    layout: PropTypes.shape({
        widget: PropTypes.string.isRequired,
        colors: PropTypes.shape(),
        orderedColors: PropTypes.bool,
        alertForMoreThan: PropTypes.number,
    }).isRequired,
    data: PropTypes.shape().isRequired,
};

export default WidgetChartPieAndDoughnut;
