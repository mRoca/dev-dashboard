import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

function WidgetLoading() {
    return <CircularProgress size={80} thickness={5} />;
}

export default WidgetLoading;
