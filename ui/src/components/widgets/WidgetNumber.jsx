import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'material-ui/Avatar';
import { grey400, green300, red300 } from 'material-ui/styles/colors';

function WidgetNumber({ data, layout }) {
    let color = grey400;

    if (typeof layout.alertForMoreThan !== 'undefined') {
        color = (data > layout.alertForMoreThan) ? red300 : green300;
    }

    return (
        <Avatar backgroundColor={color} size={100 * (layout.rows || 1)} style={{ fontSize: 80 }}>{data}</Avatar>
    );
}

WidgetNumber.propTypes = {
    layout: PropTypes.shape({
        alertForMoreThan: PropTypes.number,
        rows: PropTypes.number,
    }).isRequired,
    data: PropTypes.number.isRequired,
};

export default WidgetNumber;
