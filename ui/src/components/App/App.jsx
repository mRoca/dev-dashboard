import React from 'react';
import './App.css';
import WidgetsListContainer from '../../store/containers/WidgetsListContainer';
import LayoutContainer from '../../store/containers/LayoutContainer';

function App() {
    return (
        <div className="App">
            <LayoutContainer>
                <WidgetsListContainer />
            </LayoutContainer>
        </div>
    );
}

export default App;
