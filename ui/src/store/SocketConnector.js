import io from 'socket.io-client';
import { setSocketStatus, setWidgetData } from './actions/index';

export default class SocketConnector {
    constructor(url, options, store) {
        this.socket = io(url, options);
        this.store = store;
    }

    connect() {
        this.socket.on('connect', () => {
            this.store.dispatch(setSocketStatus('connected'));
        });

        this.socket.on('disconnect', () => {
            this.store.dispatch(setSocketStatus('disconnect'));
        });

        this.socket.on('error', (err) => {
            this.store.dispatch(setSocketStatus('failed'));
            throw new Error('error', err);
        });

        this.socket.on('reconnect', () => {
            this.store.dispatch(setSocketStatus('connected'));
        });

        this.socket.on('reconnect_attempt', () => {
            this.store.dispatch(setSocketStatus('reconnect_attempt'));
        });

        this.socket.on('reconnecting', () => {
            this.store.dispatch(setSocketStatus('reconnecting'));
        });

        this.socket.on('reconnect_failed', (error) => {
            this.store.dispatch(setSocketStatus('failed'));
            throw new Error('reconnect_failed', error);
        });

        this.socket.on('server_start', () => {
            // Reload and flushing cache at server start
            window.location.reload(true);
        });
    }

    listenStoreEvents() {
        this.socket.on('widgets:data:update', (data) => {
            Object.keys(data).forEach((widgetName) => {
                this.store.dispatch(setWidgetData(widgetName, data[widgetName]));
            });
        });
    }

    requestDataUpdate(id = null) {
        this.socket.emit('widgets:data:update_request', id);
    }

    requestDataForceUpdate(id) {
        this.socket.emit('widgets:data:force_update_request', id);
    }
}
