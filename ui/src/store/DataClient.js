import { addOrUpdateWidget } from './actions/index';

/**
 * Rest client, will be replaced by a socket.io listener
 */
export default class DataClient {
    constructor(url, store) {
        this.baseUrl = url.replace(/\/$/, '');
        this.store = store;
    }

    refreshWidgets() {
        const self = this;

        return fetch(`${this.baseUrl}/widgets`)
            .then(response => response.json())
            .then((data) => {
                Object.keys(data).forEach((widgetName) => {
                    self.store.dispatch(addOrUpdateWidget(widgetName, data[widgetName]));
                });

                return data;
            });
    }
}
