import { combineReducers } from 'redux';
import widgets from './widgets';
import socketStatus from './socketStatus';

export default combineReducers({
    widgets,
    socketStatus,
});
