const widget = (state = {}, action) => {
    switch (action.type) {
    case 'ADD_WIDGET':
        return {
            id: action.id,
            config: action.widget.config,
            layout: action.widget.layout,
            initialized: false,
            data: {},
        };
    case 'UPDATE_WIDGET':
        if (state.id !== action.id) {
            return state;
        }

        return Object.assign({}, state, {
            config: action.widget.config,
            layout: action.widget.layout,
        });
    case 'SET_WIDGET_DATA':
        if (state.id !== action.id) {
            return state;
        }

        return Object.assign({}, state, {
            initialized: true,
            data: action.data,
        });
    case 'RESET_WIDGET_DATA':
        if (state.id !== action.id) {
            return state;
        }

        return Object.assign({}, state, {
            initialized: false,
            data: {},
        });
    default:
        return state;
    }
};

const widgets = (state = [], action) => {
    switch (action.type) {
    case 'ADD_WIDGET':
        return [
            ...state,
            widget(undefined, action),
        ];
    case 'UPDATE_WIDGET':
        return state.map(t =>
            widget(t, action),
        );
    case 'ADD_OR_UPDATE_WIDGET':
        if (state.findIndex(t => t.id === action.id) > -1) {
            return widgets(state, Object.assign({}, action, { type: 'UPDATE_WIDGET' }));
        }

        return widgets(state, Object.assign({}, action, { type: 'ADD_WIDGET' }));
    case 'SET_WIDGET_DATA':
        return state.map(t =>
            widget(t, action),
        );
    case 'RESET_WIDGET_DATA':
        return state.map(t =>
            widget(t, action),
        );
    case 'RESET_WIDGETS_DATA':
        return state.map(t =>
            widget(t, Object.assign({}, action, { type: 'RESET_WIDGET_DATA', id: t.id })),
        );
    default:
        return state;
    }
};

export default widgets;
