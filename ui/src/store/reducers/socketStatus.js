const socketStatus = (state = '', action) => {
    switch (action.type) {
    case 'SET_SOCKET_STATUS':
        return action.status;
    default:
        return state;
    }
};

export default socketStatus;
