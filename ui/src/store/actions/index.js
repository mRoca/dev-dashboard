export const addOrUpdateWidget = (id, widget) => ({
    type: 'ADD_OR_UPDATE_WIDGET',
    id,
    widget,
});

export const setWidgetData = (id, data) => ({
    type: 'SET_WIDGET_DATA',
    id,
    data,
});

export const setSocketStatus = status => ({
    type: 'SET_SOCKET_STATUS',
    status,
});

export const refreshWidgetsRequest = () => ({
    type: 'REFRESH_WIDGETS_REQUEST',
});

export const refreshWidgetRequest = id => ({
    type: 'REFRESH_WIDGET_REQUEST',
    id,
});

export const resetWidgetsData = () => ({
    type: 'RESET_WIDGETS_DATA',
});

export const resetWidgetData = id => ({
    type: 'RESET_WIDGET_DATA',
    id,
});
