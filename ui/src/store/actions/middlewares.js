import { resetWidgetData, resetWidgetsData } from './index';

export const createRefreshWidgetsMiddleware = container => store => next => (action) => { // eslint-disable-line no-unused-vars, import/prefer-default-export
    next(action);

    if (action.type === 'REFRESH_WIDGETS_REQUEST') {
        container.services.socketConnector.requestDataForceUpdate();
        next(resetWidgetsData());
    }

    if (action.type === 'REFRESH_WIDGET_REQUEST') {
        container.services.socketConnector.requestDataForceUpdate(action.id);
        next(resetWidgetData(action.id));
    }
};
