import { connect } from 'react-redux';
import Layout from '../../components/Layout/Layout';
import { refreshWidgetsRequest } from '../actions/index';

const mapStateToProps = state => ({
    socketStatus: state.socketStatus,
});

const mapDispatchToProps = dispatch => ({
    onRefreshClick: () => {
        dispatch(refreshWidgetsRequest());
    },
});

const LayoutContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Layout);

export default LayoutContainer;
