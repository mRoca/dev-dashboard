import { connect } from 'react-redux';
import WidgetsList from '../../components/widgets/WidgetsList';
import { refreshWidgetRequest } from '../actions/index';

const mapStateToProps = state => ({
    widgets: state.widgets,
});

const mapDispatchToProps = dispatch => ({
    onRefreshClick: (widget) => {
        dispatch(refreshWidgetRequest(widget.id));
    },
});

const WidgetsListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(WidgetsList);

export default WidgetsListContainer;
