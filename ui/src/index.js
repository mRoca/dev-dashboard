import { createStore, applyMiddleware } from 'redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import registerServiceWorker from './helpers/registerServiceWorker';
import reducers from './store/reducers';
import DataClient from './store/DataClient';
import SocketConnector from './store/SocketConnector';
import bootstrap from './bootstrap';
import { createRefreshWidgetsMiddleware } from './store/actions/middlewares';

injectTapEventPlugin(); // See http://www.material-ui.com/#/get-started/installation

const defaultStoreData = {
    widgets: [],
    socketStatus: 'loading',
};

const container = { services: {} }; // Used to inject services in store middlewares. TODO Find another way
const store = createStore(reducers, defaultStoreData, applyMiddleware(createRefreshWidgetsMiddleware(container)));

if (!process.env.REACT_APP_SERVER_ADDRESS) {
    throw new Error('You must define a REACT_APP_SERVER_ADDRESS env variable');
}

container.services.dataClient = new DataClient(process.env.REACT_APP_SERVER_ADDRESS, store);
container.services.socketConnector = new SocketConnector(process.env.REACT_APP_SERVER_ADDRESS, { transports: ['websocket'] }, store);
container.services.socketConnector.connect();
container.services.socketConnector.listenStoreEvents();

container.services.dataClient.refreshWidgets().then(() => container.services.socketConnector.requestDataUpdate());

bootstrap(store);

registerServiceWorker();
