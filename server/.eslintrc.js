module.exports = {
  'plugins': ['node'],
  'extends': ['eslint:recommended', 'plugin:node/recommended', 'google'],
  'rules': {
    'node/exports-style': ['error', 'module.exports'],
    'indent': ['error', 4],
    'max-len': ['error', 200],
    'require-jsdoc': ['error', {
      'require': {
        'FunctionDeclaration': false,
      }
    }]
  },
  'env': {
    'node': 1
  }
};
