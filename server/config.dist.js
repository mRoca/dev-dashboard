module.exports = {
    // Constants
    PORT: 80, // Can be overriden by the PORT env variable
    DEBUG: 'app:*', // Can be overriden by the DEBUG env variable, see https://www.npmjs.com/package/debug // Set to '*' to see all logs
    WIDGET_DATA_UPDATE_TIMEOUT: 5 * 60 * 1000,

    // Project data
    providers: {
//        myGitlabProvider: {
//            type: 'gitlab',
//            config: {
//                url: 'https://*******/api/v3',
//                apiToken: '*******',
//            },
//        },
//        myIframeProvider: {
//            type: 'static',
//            config: {
//                url: 'https://mywebsite/my/page.html'
//            },
//        },
//        myJiraProvider: {
//            type: 'jira',
//            config: {
//                url: 'https://myjira.atlassian.net',
//                apiToken: '******' // base64_encode(username:password)
//            },
//        },
//        mnoGuys: {
//            type: 'static',
//            config: {
//                data: [
//                    'Joe',
//                    'William',
//                    'Jack',
//                    'Averell'
//                ],
//            },
//        },
    },
    widgets: {
//        gitlabPRNumber: {
//            provider: 'myGitlabProvider',
//            method: 'gitOpenPullRequestsNumber',
//            config: {
//                projectId: 'my/project',
//            },
//            layout: {
//                title: 'My project open PR, alert after 10',
//                widget: 'number',
//                rows: 1, // Default value
//                cols: 1, // Default value
//                gridNumber: 1, // Default value
//                alertForMoreThan: 10,
//            },
//        },
//        gitlabOpenPRStatus: {
//            provider: 'myGitlabProvider',
//            method: 'gitOpenPullRequestsStatus',
//            config: {
//                projectId: 'my/project',
//            },
//            layout: {
//                title: 'My project open PR by status',
//                widget: 'chartDoughnut',
//                gridNumber: 1,
//                cols: 1,
//                rows: 2,
//                colors: { // ['red', 'green', 'pink', 'blue', 'yellow', 'brown', 'orange', 'purple', 'amber', 'teal', 'indigo', 'lightBlue', 'deepOrange', 'grey', 'lime', 'brown', 'lightGreen']
//                    'To review': 'red',
//                    'Commented': 'orange',
//                    'Approved': 'green',
//                    'WIP': 'grey',
//                },
//                orderedColors: true, // Datasets must follow the colors keys sorting
//                alertForMoreThan: 10, // For the number at the center of the doughnut chart, if any
//            },
//        },
//        gitlabOpenPRLabels: {
//            provider: 'myGitlabProvider',
//            method: 'gitOpenPullRequestsLabels',
//            config: {
//                projectId: 'my/project',
//            },
//            layout: {
//                title: 'My project open PR by label',
//                widget: 'chartPie',
//                gridNumber: 1,
//                cols: 1,
//                rows: 2,
//                colors: {
//                    'mergeable': 'green',
//                    'need-review': 'red',
//                    'technical': 'blue',
//                    'user-story': 'grey',
//                },
//                orderedColors: true,
//                alertForMoreThan: 10,
//            },
//        },
//        sprintIframe: {
//            provider: 'myIframeProvider',
//            method: 'getUrl',
//            layout: {
//                title: 'Sprint chart',
//                widget: 'iframe',
//                cols: 2,
//                rows: 4,
//                gridNumber: 1,
//            }
//        },
//        chartOfPRNumberOverTime: {
//            provider: 'myGitlabProvider',
//            method: 'gitOpenPullRequestsNumberOverTime',
//            config: {
//                projectId: 'my/project',
//            },
//            layout: {
//                title: 'Gitlab open PR, alert after 15',
//                widget: 'chartBar',
//                rows: 2,
//                cols: 2,
//                gridNumber: 2,
//                alertForMoreThan: 15,
//            },
//        },
//        sprintTodoStories: {
//            provider: 'myJiraProvider',
//            method: 'issuesNumber',
//            config: {
//                jql: 'project = PRJ AND issuetype in (Story, Sub-task) AND sprint in openSprints() AND status in ("To Do")',
//            },
//            layout: {
//                title: 'JIRA - Current sprint TODO stories or tasks',
//                widget: 'number',
//                gridNumber: 2,
//                alertForMoreThan: 0,
//            },
//        },
//        sprintChart: {
//            provider: 'myJiraProvider',
//            method: 'issuesNumberPerField',
//            config: {
//                jql: 'project = PRJ AND issuetype in (Story, Sub-task) AND sprint in openSprints()',
//                field: 'status'
//            },
//            layout: {
//                title: 'JIRA - Sprint stories',
//                widget: 'chartPie',
//                cols: 1,
//                rows: 2,
//                gridNumber: 2,
//                colors: {
//                    'Icebox': 'red',
//                    'To Do': 'red',
//                    'In Progress': 'orange',
//                    'Code Review': 'yellow',
//                    'QA': 'yellow',
//                    'Done': 'green',
//                    'Closed': 'green',
//              },
//              orderedColors: true
//            },
//        },
//        mnoGuy: {
//            provider: 'mnoGuys',
//            method: 'getData',
//            layout: {
//                title: 'M&O guy',
//                widget: 'listItem',
//                cols: 1,
//                rows: 2,
//                gridNumber: 2,
//                duration: 'P7D',
//                startDate: '2017-09-29T14:00:00+01:00',
//                displayNext: true
//            },
//        },
    },
};
