const Widget = require('./Widget');

function WidgetFactory(registeredWidgets, providerFactory) {
    if (!registeredWidgets) {
        throw new Error('You must provide a registeredWidgets object');
    }

    if (!providerFactory) {
        throw new Error('You must provide a providerFactory instance');
    }

    this.providerFactory = providerFactory;
    this.registeredWidgets = registeredWidgets;
}

WidgetFactory.prototype.getByName = function(name) {
    if (typeof this.registeredWidgets[name] === 'undefined') {
        throw new Error('Unable to find the ' + name + ' widget in the config');
    }

    let provider = null;
    if (this.registeredWidgets[name].provider) {
        provider = this.providerFactory.getByName(this.registeredWidgets[name].provider);
    }

    const config = Object.assign({providerName: this.registeredWidgets[name].provider}, this.registeredWidgets[name].config || {});
    return new Widget(name, provider, this.registeredWidgets[name].method || null, config);
};

WidgetFactory.prototype.getAll = function() {
    return Object.keys(this.registeredWidgets).map(this.getByName.bind(this));
};

module.exports = WidgetFactory;
