const Promise = require('bluebird');

function WidgetManager(widgetFactory) {
    this.widgetFactory = widgetFactory;
}

/**
 * @return {Promise}
 */
WidgetManager.prototype.getWidgetsStates = function() {
    const widgets = this.widgetFactory.getAll();
    const promises = {};

    widgets.forEach(function(widget) {
        promises[widget.name] = widget.getPromisedData();
    });

    return Promise.props(promises);
};

/**
 * @param {string} id
 * @return {Promise}
 */
WidgetManager.prototype.getWidgetState = function(id) {
    return this.widgetFactory.getByName(id).getPromisedData();
};

module.exports = WidgetManager;
