const Promise = require('bluebird');

function Widget(name, provider, dataMethod, config) {
    if (!provider) {
        throw new Error('No provider found for the widget');
    }
    if (!config) {
        throw new Error('You must provide a config object');
    }

    this.name = name;
    this.provider = provider;
    this.dataMethod = dataMethod;
    this.config = config;
}

/**
 * @return {Promise}
 */
Widget.prototype.getPromisedData = function() {
    if (!this.dataMethod) {
        return Promise.resolve(null);
    }

    if (!this.provider[this.dataMethod]) {
        throw new Error('The provider does not have the required method');
    }

    const res = this.provider[this.dataMethod](this.config);

    if (typeof res.then === 'undefined') {
        return Promise.resolve(res);
    }

    return res;
};

module.exports = Widget;
