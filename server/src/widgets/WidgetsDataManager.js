const Promise = require('bluebird');

const debug = require('debug')('app:data');
const dataPromisesTimeout = 5 * 60 * 1000;

function WidgetsDataManager(cache) {
    this.cache = cache;
    this.runningUpdates = {};
}

/**
 * @param {Widget} widget
 * @param {boolean} refreshCache
 * @return {Promise}
 */
WidgetsDataManager.prototype.getWidgetDataPromise = function(widget, refreshCache = false) {
    const self = this;

    /**
     * @param {Widget} widget
     * @return {Promise}
     */
    function getNonConcurrentDataForWigdet(widget) {
        if (typeof self.runningUpdates[widget.name] !== 'undefined') {
            debug('Yet getting data for widget ' + widget.name + ', waiting for the first result');

            return self.runningUpdates[widget.name];
        }

        return self.runningUpdates[widget.name] = widget.getPromisedData()
            .timeout(dataPromisesTimeout)
            .catch(Promise.TimeoutError, function() {
                debug('Error: could not resolve promise for ' + widget.name + ' within ' + dataPromisesTimeout + 'ms');
                return Promise.reject();
            })
            .catch(function(e) {
                debug('Error during ' + widget.name + ' data promise : ' + e.message);
                return Promise.reject();
            })
            .finally(function() {
                delete self.runningUpdates[widget.name];
            });
    }

    if (refreshCache) {
        debug('Deleting cached data for widget ' + widget.name);
        self.cache.del('data:' + widget.name);
    }

    return new Promise(function(resolve, reject) {
        debug('Getting data from cache for widget ' + widget.name);

        self.cache.get('data:' + widget.name).then(function(data) {
            if (typeof data === 'undefined') {
                debug('No cached data, getting new data for widget ' + widget.name);

                return getNonConcurrentDataForWigdet(widget)
                    .then(function(data) {
                        self.cache.set('data:' + widget.name, data);
                        resolve(data);
                    })
                    .catch(reject)
                    .finally(reject);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = WidgetsDataManager;
