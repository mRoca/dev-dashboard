function StaticProvider(config) {
    if (typeof config !== 'object') {
        throw new Error('Bad parameters');
    }

    this.config = config;

    const self = this;
    Object.keys(config).forEach(function(property) {
        self['get' + property.charAt(0).toUpperCase() + property.slice(1)] = function() {
            return self.config[property];
        }
    });
}

module.exports = StaticProvider;
