function ProviderFactory(registeredProviders) {
    if (!registeredProviders) {
        throw new Error('You must provide a registeredProviders object');
    }

    this.registeredProviders = registeredProviders;
    this.providersClasses = this.getAvailableProviders();
}

ProviderFactory.prototype.getAvailableProviders = function() {
    return {
        'gitlab': require('./GitlabClient'),
        'jira': require('./JiraClient'),
        'static': require('./StaticProvider'),
    };
};

ProviderFactory.prototype.getByType = function(type, config) {
    if (typeof this.providersClasses[type] === 'undefined') {
        throw new Error('Unable to find the ' + type + ' provider class');
    }

    return new this.providersClasses[type](config);
};

ProviderFactory.prototype.getByName = function(name) {
    if (typeof this.registeredProviders[name] === 'undefined') {
        throw new Error('Unable to find the ' + name + ' provider in the config');
    }

    const registeredProvider = this.registeredProviders[name];

    return this.getByType(registeredProvider.type, registeredProvider.config);
};

module.exports = ProviderFactory;
