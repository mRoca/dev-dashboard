const rp = require('request-promise');
const moment = require('moment');
const debug = require('debug');
const logRequest = debug('app:http:req');
const logResponse = debug('app:http:res');
const logError = debug('app:http:err');

const RED = 31;
const GREEN = 32;

function debugColor(str, code = RED) {
    if (!debug.useColors()) {
        return str;
    }

    return '\u001b[' + code + 'm' + str + '\u001b[0;00m';
}

function HttpHelper(baseUrl, headers = {}) {
    this.baseUrl = baseUrl.replace(/\/$/, '');
    this.headers = headers;
}

HttpHelper.prototype.req = function(options) {
    const startDate = moment();

    logRequest(options.uri);

    const r = rp(options)
        .catch(function(reason) {
            logError(debugColor(reason.message, RED) + ' ' + options.uri);
            r.cancel();
        })
        .then(function(response) {
            const timeSpent = moment().diff(startDate);
            logResponse(debugColor(response.statusCode + ' ' + timeSpent + 'ms ', GREEN) + options.uri);

            return response;
        })
    ;

    return r;
};

HttpHelper.prototype.get = function(uri) {
    return this.req({
        uri: this.baseUrl + uri,
        headers: this.headers,
        json: true,
        resolveWithFullResponse: true,
    });
};

HttpHelper.prototype.post = function(uri, body) {
    return this.req({
        method: 'POST',
        uri: this.baseUrl + uri,
        body,
        headers: this.headers,
        json: true,
        resolveWithFullResponse: true,
    });
};

HttpHelper.prototype.buildUri = function(uri, options) {
    Object.keys(options).forEach(function(key) {
        uri = uri.replace(new RegExp(':' + key + ':', 'g'), encodeURIComponent(options[key]));
    });

    return uri;
};

module.exports = HttpHelper;
