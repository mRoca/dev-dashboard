const moment = require('moment');
const MOMENT_DAY_FORMAT = 'YYYY-MM-DD';

const HttpHelper = require('./HttpHelper');

function GitlabClient(config) {
    if (!config.url || !config.apiToken) {
        throw new Error('Bad Gitlab parameters');
    }

    this.httpHelper = new HttpHelper(config.url, {'PRIVATE-TOKEN': config.apiToken});
    this.minPerPage = 1; // Some Gitlab versions don't work with per_page=0
    this.maxPerPage = 100;
}

GitlabClient.prototype.gitPullRequestsNumber = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    return this.httpHelper
        .get(this.httpHelper.buildUri('/api/v3/projects/:projectId:/merge_requests?per_page=:perPage:', {projectId, perPage: this.minPerPage}))
        .then(function(response) {
            if (!response.headers['x-total']) {
                throw new Error('Bad api response');
            }

            return parseInt(response.headers['x-total'], 10);
        });
};

GitlabClient.prototype.gitOpenPullRequestsNumber = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    return this.httpHelper
        .get(this.httpHelper.buildUri('/api/v3/projects/:projectId:/merge_requests?state=opened&per_page=:perPage:', {projectId, perPage: this.minPerPage}))
        .then(function(response) {
            if (!response.headers['x-total']) {
                throw new Error('Bad api response');
            }

            return parseInt(response.headers['x-total'], 10);
        });
};

GitlabClient.prototype.gitOpenPullRequestsStatus = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    return this._getAllPullRequests(projectId, true).then(function(pullRequests) {
        const res = {};

        function addRes(key) {
            if (!res[key]) {
                res[key] = 0;
            }

            res[key]++;
        }

        pullRequests.forEach(function(pr) {
            if (pr.work_in_progress) {
                return addRes('WIP');
            }

            if (pr.upvotes > 0) {
                return addRes('Approved');
            }

            if (pr.user_notes_count > 0) {
                return addRes('Commented');
            }

            addRes('To review');
        });

        return res;
    });
};

GitlabClient.prototype.gitOpenPullRequestsLabels = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    return this._getAllPullRequests(projectId, true).then(function(pullRequests) {
        const res = {};

        function addRes(key) {
            if (!res[key]) {
                res[key] = 0;
            }

            res[key]++;
        }

        pullRequests.forEach(function(pr) {
            pr.labels.forEach(addRes);
        });

        return res;
    });
};

GitlabClient.prototype.gitOpenPullRequestsWithoutApprovalNumber = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    return this._getAllPullRequests(projectId, true).then(function(pullRequests) {
        return pullRequests.filter(function(pr) {
            return !pr.upvotes;
        }).length;
    });
};


GitlabClient.prototype.gitOpenPullRequestsNumberOverTime = function(config) {
    const projectId = config.projectId;

    if (!config.projectId) {
        throw new Error('Invalid argument');
    }

    const enumerateDaysBetweenDates = function(startDate, endDate) {
        const dates = [];

        const currDate = startDate.clone().startOf('day');
        const lastDate = endDate.clone().startOf('day');

        do {
            dates.push(currDate.clone());
        } while (currDate.add(1, 'days').diff(lastDate) <= 0);

        return dates;
    };

    return this._getAllPullRequests(projectId, false).then(function(pullRequests) {
        const res = {};

        pullRequests.forEach(function(pr) {
            let days;

            if (pr.state === 'opened') {
                days = enumerateDaysBetweenDates(moment(pr.created_at), moment());
            } else {
                days = enumerateDaysBetweenDates(moment(pr.created_at), moment(pr.closed_at || pr.updated_at));
            }

            days.forEach(function(day) {
                if (!res[day.format(MOMENT_DAY_FORMAT)]) {
                    res[day.format(MOMENT_DAY_FORMAT)] = 0;
                }

                res[day.format(MOMENT_DAY_FORMAT)]++;
            });
        });

        const ordered = {};
        Object.keys(res).sort().forEach(function(key) {
            ordered[key] = res[key];
        });

        return ordered;
    });
};

GitlabClient.prototype._getAllPullRequests = function(projectId, onlyOpen = false) {
    const url = '/api/v3/projects/:projectId:/merge_requests?per_page=:perPage:&page=:pageIndex:' + (onlyOpen ? '&state=opened' : '');
    const data = [];

    const getData = function(page) {
        return this.httpHelper
            .get(this.httpHelper.buildUri(url, {projectId, perPage: this.maxPerPage, pageIndex: (page || '1')}))
            .then(function(response) {
                data.push(...response.body);

                if (response.headers['x-next-page']) {
                    return getData(response.headers['x-next-page']);
                }

                return data;
            });
    }.bind(this);

    return getData();
};

module.exports = GitlabClient;
