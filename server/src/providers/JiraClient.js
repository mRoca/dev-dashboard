const HttpHelper = require('./HttpHelper');

function JiraClient(config) {
    if (!config.url || !config.apiToken) {
        throw new Error('Bad Gitlab parameters');
    }

    this.httpHelper = new HttpHelper(config.url, {'Authorization': 'Basic ' + config.apiToken});
    this.minPerPage = 0; // Some Gitlab versions don't work with per_page=0
    this.maxPerPage = 500;
}

JiraClient.prototype.issuesNumber = function(config) {
    const jql = config.jql;

    if (!config.jql) {
        throw new Error('Invalid argument');
    }

    return this.httpHelper
        .post('/rest/api/2/search', {jql, fields: [], maxResults: this.minPerPage})
        .then(function(response) {
            if (!response.body.total) {
                throw new Error('Bad api response');
            }

            return parseInt(response.body.total, 10);
        });
};

JiraClient.prototype.issuesNumberPerField = function(config) {
    const jql = config.jql;
    const field = config.field;

    if (!config.jql || !config.field) {
        throw new Error('Invalid argument');
    }

    return this._getAllIssues(jql, [field]).then(function(issues) {
        const res = {};

        function addRes(val) {
            const f = val.name || val;
            res[f] || (res[f] = 0);
            res[f]++;
        }

        issues.forEach(function(issue) {
            if (Array.isArray(issue.fields[field])) {
                issue.fields[field].forEach(addRes);
            } else {
                addRes(issue.fields[field]);
            }
        });

        return res;
    });
};


JiraClient.prototype._getAllIssues = function(jql, fields) {
    const data = [];

    const getData = function(startAt = 0) {
        return this.httpHelper
            .post('/rest/api/2/search', {jql, fields, maxResults: this.maxPerPage, startAt})
            .then(function(response) {
                data.push(...response.body.issues);

                if (response.body.total > response.body.startAt + response.body.maxResults) {
                    return getData(response.body.startAt + response.body.maxResults);
                }

                return data;
            });
    }.bind(this);

    return getData();
};

module.exports = JiraClient;
