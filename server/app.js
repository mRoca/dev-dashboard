const fs = require('fs');

// ==============
// ===== CONFIG
// ==============

if (!fs.existsSync('./config.js')) {
    throw new Error('Unable to find the config.js file.');
}

const config = require('./config'); // eslint-disable-line node/no-unpublished-require
const distConfig = require('./config.dist');

const missingConfigKeys = Object.keys(distConfig).filter(function(x) {
    return Object.keys(config).indexOf(x) < 0;
});

if (missingConfigKeys.length) {
    throw new Error('The config.js should contain the following keys : ' + missingConfigKeys.join(', '));
}

const PORT = parseInt(process.env.PORT, 10) || config.PORT || 3000;
process.env.DEBUG = process.env.DEBUG || config.DEBUG || '*'; // Must be before the first require('debug') call
process.env.DEBUG_COLORS = true;

// ====================
// ===== Dependancies
// ====================

const app = require('express')();
const server = require('http').Server(app); // eslint-disable-line new-cap
const io = require('socket.io')(server);
const morgan = require('morgan');
const cacheManager = require('cache-manager');
const ProviderFactory = require('./src/providers/ProviderFactory');
const WidgetFactory = require('./src/widgets/WidgetFactory');
const WidgetsManager = require('./src/widgets/WidgetsManager');
const WidgetsDataManager = require('./src/widgets/WidgetsDataManager');
const Promise = require('bluebird');

const debug = require('debug')('app:global');
const cache = cacheManager.caching({store: 'memory', promiseDependency: Promise, max: 100, ttl: config.WIDGET_DATA_UPDATE_TIMEOUT / 1000});

// ==============
// ===== Managers
// ==============

const providerFactory = new ProviderFactory(config.providers);
const widgetFactory = new WidgetFactory(config.widgets, providerFactory);
const widgetsManager = new WidgetsManager(widgetFactory);
const widgetsDataManager = new WidgetsDataManager(cache);

// ==============
// ===== REST API
// ==============

app.use(morgan('dev'));

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/', function(req, res) {
    res.json({success: true});
});

app.get('/widgets', function(req, res) {
    res.json(config.widgets);
});

function providerRefreshEndpoint(req, res) {
    const widgets = widgetFactory.getAll().filter(function(widget) {
        return widget.config.providerName === req.params.provider;
    });

    widgets.forEach(function(widget) {
        refreshWidgetData(widget.name, false);
    });

    res.json({
        refreshedWidgets: widgets.map(function(widget) {
            return widget.name;
        }),
    });
}

function widgetRefreshEndpoint(req, res) {
    const ok = refreshWidgetData(req.params.id, false);

    res.json({
        refreshedWidgets: ok ? [req.params.id] : [],
    });
}

app
    .get('/provider/:provider/refresh', providerRefreshEndpoint)
    .post('/provider/:provider/refresh', providerRefreshEndpoint);

app
    .get('/widget/:id/refresh', widgetRefreshEndpoint)
    .post('/widget/:id/refresh', widgetRefreshEndpoint);

app.get('/widgets/state', function(req, res) {
    widgetsManager.getWidgetsStates().then(function(data) {
        res.json(data);
    });
});

app.get('/widgets/:id/state', function(req, res) {
    widgetsManager.getWidgetState(req.params.id).then(function(data) {
        res.json(data);
    });
});


// ==============
// ==== SOCKET.IO
// ==============

io.on('connection', function(socket) {
    debug('User connected');

    socket.on('disconnect', () => {
        debug('User disconnected');
    });

    socket.on('widgets:data:update_request', (id) => {
        if (!id) {
            refreshAllWidgetsData();
        } else {
            refreshWidgetData(id);
        }
    });

    socket.on('widgets:data:force_update_request', (id) => {
        if (!id) {
            refreshAllWidgetsData(false);
        } else {
            refreshWidgetData(id, false);
        }
    });
});

setTimeout(function() {
    io.sockets.emit('server_start');
}, 5000);

// ==============
// ==== Run
// ==============

server.listen(PORT, function() {
    debug('App listening on port ' + PORT);
});


// ==============
// ==== Helpers
// ==============

const runningUpdates = {};
let nextRefreshWidgetsDataTimemout = null;

function setRefreshWidgetsDataTimeout() {
    nextRefreshWidgetsDataTimemout = setTimeout(refreshAllWidgetsData, config.WIDGET_DATA_UPDATE_TIMEOUT);
}

function updateDataForWidget(widget, useCache = true) {
    debug('Getting data for widget ' + widget.name);

    if (typeof runningUpdates[widget.name] !== 'undefined') {
        debug('Yet getting data for widget ' + widget.name);
        return;
    }

    return runningUpdates[widget.name] = widgetsDataManager.getWidgetDataPromise(widget, !useCache)
        .then(function(data) {
            debug('Broadcasting data for widget ' + widget.name);

            const res = {};
            res[widget.name] = data;

            io.sockets.emit('widgets:data:update', res);
        })
        .catch(function() {
            // Unable to retrieve data
        })
        .finally(function() {
            delete runningUpdates[widget.name];
        });
}

function refreshAllWidgetsData(useCache = true) {
    nextRefreshWidgetsDataTimemout && clearTimeout(nextRefreshWidgetsDataTimemout);

    io.sockets.clients(function(error, clients) {
        if (!clients.length) {
            return setRefreshWidgetsDataTimeout();
        }

        if (error) throw error;

        debug('Updating all widgets data for ' + clients.length + ' connected clients');

        const promises = [];
        widgetFactory.getAll().forEach(function(widget) {
            promises.push(updateDataForWidget(widget, useCache));
        });

        Promise.all(promises).finally(setRefreshWidgetsDataTimeout);
    });
}

function refreshWidgetData(id, useCache = true) {
    try {
        const widget = widgetFactory.getByName(id);
        updateDataForWidget(widget, useCache);
    } catch (e) {
        return false;
    }

    return true;
}
