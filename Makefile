
all: configure build-docker-update build start

configure:
	test -f ./server/config.js || cp ./server/config.dist.js ./server/config.js

unconfigure:
	rm -f ./server/config.js

build:
	docker-compose run --rm ui yarn install --pure-lockfile
	docker-compose run --rm server yarn install --pure-lockfile

build-prod: build
	docker-compose run --rm ui yarn build

build-docker-update:
	docker-compose pull

start:
	docker-compose up -d ui server
	@echo "Done : http://ui.devdashboard.docker/"

start-prod:
	docker-compose up -d ui_prod
	@echo "Done : http://ui_prod.devdashboard.docker/"

run: start

logs:
	docker-compose logs -f

stop:
	docker-compose stop

clean: stop
	docker-compose down -v --remove-orphans || true

restart: clean start logs

test-cs:
	docker-compose run --rm ui ./node_modules/.bin/eslint --ext .js --ext .jsx --ignore-path .gitignore --cache app.js ./src
	docker-compose run --rm server ./node_modules/.bin/eslint --ignore-path .gitignore --cache ./src

fix-cs:
	docker-compose run --rm ui ./node_modules/.bin/eslint --ext .js --ext .jsx --ignore-path .gitignore --cache --fix ./src || true
	docker-compose run --rm server ./node_modules/.bin/eslint --ignore-path .gitignore --cache --fix app.js ./src || true
