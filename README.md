# Dev-dashboard-lite

This is a little dashboard project made with node.js and React. 

For a stronger app, you should use [Mozaïk](http://mozaik.rocks/).


# Install

```bash
make configure
# Now, update the server/config.js file
make build-docker
```

# Start

```bash
make start
```

Then go to http://ui.devdashboard.docker

# Dev

## Show logs

```bash
make logs
```

## Coding style

See https://github.com/airbnb/javascript/tree/master/react for the UI part coding style,
and https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb/rules/react.js for eslint rules

**Check the cs**

```bash
make test-cs
```

**Fix the cs**

```bash
make fix-cs
```

## Run a command in containers

```bash
# ui
docker-compose run --rm ui the_command

# server
docker-compose run --rm server the_command
```

### Add a package

```bash
# ui
docker-compose run --rm ui yarn add package-name

# server
docker-compose run --rm server yarn add package-name
```
